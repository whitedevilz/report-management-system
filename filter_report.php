<?php

include "src/config.php";
if (isset($_POST['request'])) {
  $request = $_POST['request'];

  if ($request == 0) {
    $query = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id";
  }
  else {
    $query = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE teams.team_id = '$request'";
  }

  $result = mysqli_query($conn, $query);
  $count = mysqli_num_rows($result);

?>

<table class="table">

<?php
  if ($count) {
    ?>

    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Date</th>
        <th scope="col">Employee Name</th>
        <th scope="col">Project Name</th>
        <th scope="col">Tasks</th>
        <th scope="col">Hrs</th>
      </tr>
      <?php
    }else {
      echo "No Record Found!";
    }
      ?>
    </thead>
    <tbody>
      <?php
        while ($row = mysqli_fetch_assoc($result)) {
      ?>
      <tr>
        <td scope="col">#</td>
        <td scope="col"><?php echo $row['date']; ?></td>
        <td scope="col"><a href="single_user_reports.php?id=<?php echo $row['id']?>&user=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a></td>
        <td scope="col"><a href="single_project_report.php?pid=<?php echo $row['projects_id']; ?>&pname=<?php echo $row['projects_name']; ?>"><?php echo $row['projects_name']; ?></a></td>
        <td scope="col"><?php echo $row['tasks']; ?></td>
        <td scope="col"><?php echo $row['hrs']." hrs"; ?></td>
      </tr>
      <?php
        }
      ?>
    </tbody>
</table>
<?php } ?>
