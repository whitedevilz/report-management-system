<?php

    include_once ("src/header.php");

    include_once ("header.php");

    $query = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id WHERE reports.report_id = $_GET[id]";
    $results = mysqli_query($conn, $query);

    if($results){
        while($row = $results->fetch_assoc()){
            $date = $row['date'];
            $tasks = $row['tasks'];
            $hrs = $row['hrs'];
            $project = $row['projects_name'];
            $project_id = $row['projects_id'];
        }
    }
?>



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Edit Report</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class=" m-auto">
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="date">Date</label>
                                                <input type="date" class="form-control" id="date" name="date" value="<?php echo $date; ?>">
                                            </div>
                                            <div class="form-group">
                                                <label for="project_name">Project Name</label>
                                                <select id="project_name" name="project_name" class="form-control">
                                                    <option disabled>Select One</option>
                                                    <?php
                                                        $sql1 = "SELECT * FROM projects";
                                                        $result1 = mysqli_query($conn, $sql1);
                                                        while($row = $result1->fetch_assoc()) {
                                                          if ($row['projects_id'] == $project_id) {
                                                            ?>
                                                            <option selected value="<?php echo $project_id; ?>"><?php echo $project ?></option>
                                                            <?php
                                                          }
                                                          else{
                                                            echo '<option value="'.$row['projects_id'].'">'.$row['projects_name'].'</option>';
                                                          }
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="tasks">Tasks</label>
                                                <textarea class="form-control" id="tasks" name="tasks" rows="4"><?php echo $tasks; ?></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="hrs">Hrs</label>
                                                <input type="number" class="form-control" id="hrs" name="hrs" value="<?php echo $hrs; ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" name="update_report" class="btn btn-dark" value="Update Report">
                                            </div>
                                        </form>

                                        <!-- PHP -->
                                        <?php

                                        if (isset($_POST['update_report'])) {
                                            include "src/config.php";

                                            $date = $_POST["date"];
                                            $project_name = $_POST["project_name"];
                                            $tasks = $_POST["tasks"];
                                            $hrs = $_POST["hrs"];

                                            if (!empty($date) || !empty($pro_name) || !empty($tasks) || !empty($hrs)) {
                                                $sql = "UPDATE reports SET date='$date',project_name='$project_name',tasks='$tasks',hrs='$hrs',report_status=1 WHERE report_id = $_GET[id]";

                                                $result = mysqli_query($conn, $sql) or die("Query Failed");

                                                if ($result) {
                                                    echo '<div class="alert alert-success">Report Updated.</div>';
                                                }
                                                else {
                                                    echo '<div class="alert alert-danger">All Fields Required.</div>';
                                                }

                                            }
                                            else {
                                                echo '<div class="alert alert-danger">All Fields Required.</div>';
                                            }
                                        }

                                        ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
