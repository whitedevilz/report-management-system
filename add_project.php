<?php

    include_once ("src/header.php");

    if (isset($_SESSION['role']) && $_SESSION['role'] != 1) {
      header ("location: dashboard.php");
      die();
    }
    include_once ("header.php");



?>



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Add Project</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class=" m-auto">
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="date">Date</label>
                                                <input type="date" class="form-control" id="date" name="date">
                                            </div>
                                            <div class="form-group">
                                                <label for="pro_name">Project Name</label>
                                                <input type="text" name="project_name" id="pro_name" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="hrs">Hrs</label>
                                                <input type="number" class="form-control" id="hrs" name="hrs">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" name="add_project" class="btn btn-dark" value="Add Project">
                                            </div>
                                        </form>

                                        <!-- PHP -->
                                        <?php

                                        if (isset($_POST['add_project'])) {
                                            include "src/config.php";

                                            $date = $_POST["date"];
                                            $pro_name = $_POST["project_name"];
                                            $hrs = $_POST["hrs"];

                                            if (!empty($date) || !empty($pro_name) || !empty($tasks) || !empty($hrs)) {
                                                $sql = "INSERT INTO projects(project_date, projects_name, pro_hrs) VALUES ('$date','$pro_name','$hrs')";

                                                $result = mysqli_query($conn, $sql) or die("Query Failed");

                                                if ($result) {
                                                    echo '<div class="alert alert-success">New Project Added.</div>';
                                                }
                                                else {
                                                    echo '<div class="alert alert-danger">All Fields Required.</div>';
                                                }

                                            }
                                            else {
                                                echo '<div class="alert alert-danger">All Fields Required.</div>';
                                            }
                                        }

                                        ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
