<?php

include_once ("src/config.php");
include_once ("src/header.php");

if (!empty($_FILES) && isset($_FILES['fileToUpload'])) {
    switch ($_FILES['fileToUpload']["error"]) {
        case UPLOAD_ERR_OK:
            $target = "src/upload/";
            $target = $target . basename($_FILES['fileToUpload']['name']);

            if (move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target)) {
                $status = "The file " . basename($_FILES['fileToUpload']['name']) . " has been uploaded";
                $imageFileType = pathinfo($target, PATHINFO_EXTENSION);
                $check = getimagesize($target);
                if ($check !== false) {
                    $name = $_FILES['fileToUpload']['name'];
                    $id = $_SESSION['userid'];
                    $sql = "UPDATE user SET img='$name' WHERE id = $id";
                    $result = mysqli_query($conn, $sql);
                    if ($result) {
                      $uploadOk = 1;
                      header("location: profile.php");
                    }

                } else {
                    echo "File is not an image.<br>";
                    $uploadOk = 0;
                }

            } else {
                $status = "Sorry, there was a problem uploading your file.";
            }
            break;

    }

    echo "Status: {$status}<br/>\n";

}

?>
