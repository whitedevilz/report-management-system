<?php

    include_once ("src/header.php");

    include_once ("header.php");


    if ($_SESSION['role'] == 2) {
      $sql = "SELECT notification_message, date, report_status FROM report_status INNER JOIN notifications ON notifications.notification_id = report_status.status_notification INNER JOIN reports ON reports.report_status = report_status.status_id ORDER BY reports.date DESC";
    }
    else {
      $sql = "SELECT * FROM notifications INNER JOIN user ON user.notifi_id = notifications.notification_id ORDER BY user.join_date DESC";
    }
    $result = mysqli_query($conn, $sql);
?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">All Notificaions</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <table class="table table-striped" id="table_id">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Date</th>
                                          <th scope="col">Notificaion</th>
                                          <th></th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <!-- if super admin -->
                                        <?php if ($_SESSION['role'] == 1){
                                          if ($result) {
                                            while ($row = $result->fetch_assoc()) {
                                        ?>
                                              <tr>
                                                <th scope="row" class="rowNo"></th>
                                                <td scope="row">Date</td>
                                                <td scope="row">
                                                  <?php if ($row['notification_id'] == 4) {
                                                    echo 'New Employee added <a href="#"> '. $row['username'] . '</a>';
                                                  }
                                                  else {
                                                  echo $row['notification_message'] . '<a href="#"> ' . $row['username'] . '</a>';
                                                  }
                                                  ?>
                                                </td>
                                                <td scope="row" class="text-center">
                                                  <a href="#" class="btn btn-danger btn-sm">Delete</a>
                                                </td>
                                              </tr>
                                        <?php
                                            }
                                          }
                                        } ?>

                                        <!-- if team leader -->
                                        <?php if ($_SESSION['role'] != 1){
                                          if ($result) {
                                            while ($row = $result->fetch_assoc()) {
                                        ?>
                                              <tr>
                                                <th scope="row" class="rowNo"></th>
                                                <td scope="row"><?php if($_SESSION['role'] == 2){ echo $row['date']; } else {echo "Date";} ?></td>
                                                <td scope="row">
                                                  <?php echo $row['notification_message']; if ($_SESSION['role'] == 3) { ?> <a href="#"><?php echo $row['username']; ?></a> <?php } ?></td>
                                                <td scope="row" class="text-center">
                                                  <?php if($row['report_status'] == 2){ echo '<i class="fa fa-check text-success" aria-hidden="true"></i>';}
                                                  elseif ($row['report_status'] == 3) {echo '<i class="fa fa-exclamation-triangle text-danger" aria-hidden="true"></i>';}
                                                  ?>
                                                </td>
                                              </tr>
                                        <?php
                                            }
                                          }
                                        } ?>
                                      </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
