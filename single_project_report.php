<?php

    include_once ("src/header.php");

    include_once ("header.php");
    if($_SESSION['role'] == 1){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE report_status.status_id = 2 AND projects.projects_id = $_GET[pid] ORDER BY reports.date DESC";
    }
    if($_SESSION['role'] == 3){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id  WHERE reports.team_id = $_SESSION[team] AND report_status.status_id = 2 AND projects.projects_id = $_GET[pid] ORDER BY reports.date DESC";
    }
    if($_SESSION['role'] == 2){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE reports.team_id = $_SESSION[team] AND reports.role_id = $_SESSION[role] AND projects.projects_id = $_GET[pid] ORDER BY reports.date DESC";
    }
    $result = mysqli_query($conn, $sql);

?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800"><?php echo $_GET['pname']; ?></h1>
                        <a href="export.php"><button type="button" class="btn btn-primary">Export</button></a>
                    </div>

                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                      <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-all_report" role="tab" aria-controls="pills-home" aria-selected="true">All Reports</a>
                      </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                      <div class="tab-pane fade show active" id="pills-all_report" role="tabpanel" aria-labelledby="pills-home-tab">
                        <!-- Content Row -->
                        <div class="row">

                            <!-- Content Column -->
                            <div class="col-md-12 mb-4">

                                <!-- Project Card Example -->
                                <div class="card shadow mb-4">
                                    <div class="card-body">
                                        <table class="table table-striped" id="table_id">
                                          <thead>
                                            <tr>
                                              <th scope="col">#</th>
                                              <th scope="col">Date</th>
                                                <?php
                                                    if($_SESSION['role'] == 1){
                                                ?>
                                                <th scope="col">Employee Name</th>
                                                <?php
                                                    }
                                                ?>
                                                <?php
                                                    if($_SESSION['role'] == 3){
                                                ?>
                                                <th scope="col">Team Member</th>
                                                <?php
                                                    }
                                                ?>
                                              <th scope="col">Project Name</th>
                                              <th scope="col">Tasks</th>
                                               <th scope="col">Hrs</th>
                                                <?php
                                                    if($_SESSION['role'] == 2){
                                                ?>
                                               <th>Status</th>
                                                <th>Action</th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                                if($result) {
                                                    while($row = $result->fetch_assoc()){
                                            ?>
                                              <tr>
                                              <th scope="row" class="rowNo"></th>
                                              <td><?php echo $row['date']; ?></td>
                                              <?php
                                                if($_SESSION['role'] == 3 || $_SESSION['role'] == 1){
                                                ?>
                                                <td scope="col"><a href="single_user_project_report.php?uname=<?php echo $row['username']; ?>&pro_name=<?php echo $row['projects_name']; ?>&proid=<?php echo $row['projects_id']; ?>"><?php echo $row['username']; ?></a></td>
                                                <?php
                                                    }
                                                ?>
                                              <td><?php echo $row['projects_name']; ?></td>
                                              <td><?php echo $row['tasks']; ?></td>
                                              <td><?php echo $row['hrs']." hrs"; ?></td>
                                                  <?php
                                                    if($_SESSION['role'] == 2){
                                                ?>
                                                <?php
                                                  if ($row['status_id'] == 1) {
                                                ?>
                                                  <td class="text-warning text-capitalize"><i class="fa fa-exclamation-circle"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                  if ($row['status_id'] == 2){
                                                ?>
                                                  <td class="text-success text-capitalize"><i class="fa fa-check-circle"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                ?>
                                                <?php
                                                  if ($row['status_id'] == 3){
                                                ?>
                                                  <td class="text-danger text-capitalize"><i class="fa fa-ban"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                ?>
                                              <td class="text-center">
                                                <?php if ($row['status_id'] == 2){ ?>
                                                  <button class="btn btn-sm btn-primary" disabled>Edit</button>
                                                  <button class="btn btn-sm btn-danger" disabled>Delete</button>
                                              <?php }else{ ?>
                                                  <a href="edit_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-primary">Edit</a>
                                                  <a href="delete_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-danger">Delete</a>
                                                <?php } ?>
                                              </td>
                                                  <?php
                                                    }
                                                ?>
                                            </tr>

                                              <?php

                                                    }
                                                }
                                              ?>
                                          </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                      </div>
                    </div>



                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
