<?php

    include_once ("src/header.php");
    include_once ("header.php");


    include "src/config.php";
    $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN report_status ON reports.report_status = report_status.status_id INNER JOIN user ON user.id = reports.user_id WHERE report_status.status_id = 2 AND reports.role_id = 3 ORDER BY reports.date DESC";

    $result = mysqli_query($conn, $sql);

?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800 text-capitalize">My Reports</h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Content Column -->
            <div class="col-md-12 mb-4">

                <!-- Project Card Example -->
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <table class="table table-striped" id="table_id">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Date</th>
                              <th scope="col">Project Name</th>
                              <th scope="col">Report</th>
                              <th scope="col">hrs</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php

                              if ($result) {
                                while ($row = $result->fetch_assoc()) {
                          ?>
                            <tr>
                              <th scope="row" class="rowNo"></th>
                              <td class="text-capitalize"><?php echo $row['date']; ?></td>
                              <td><a href="single_user_project_report.php?uname=<?php echo $row['username']; ?>&pro_name=<?php echo $row['projects_name']; ?>&proid=<?php echo $row['projects_id']; ?>"><?php echo $row['projects_name']; ?></a></td>
                              <td class="text-capitalize"><?php echo $row['tasks']; ?></td>
                              <td class="text-capitalize"><?php echo $row['hrs']; ?></td>
                              <!-- <td>
                                <a href="edit_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-primary">Edit</a>
                                <a href="delete_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-danger">Delete</a>
                              </td> -->
                            </tr>
                          <?php
                                    }

                                    /* free result set */
                                    $result->free();
                                }

                           ?>
                          </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
