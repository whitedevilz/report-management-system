-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2022 at 10:22 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `report-managment`
--
CREATE DATABASE IF NOT EXISTS `report-managment` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `report-managment`;

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `notification_id` int(11) NOT NULL,
  `notification` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`notification_id`, `notification`) VALUES
(1, 'new report added'),
(2, 'new team member added'),
(3, 'new employee added'),
(4, 'report rejected'),
(5, 'report approved'),
(6, 'new project assigned');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `projects_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `projects_name` varchar(255) NOT NULL,
  `pro_hrs` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`projects_id`, `date`, `projects_name`, `pro_hrs`) VALUES
(4, '2022-02-23', 'GoServe', 400),
(5, '2022-02-22', 'Fahrschulestar', 300),
(6, '2022-02-21', 'Industree', 200),
(7, '2022-02-19', 'ComeOnNow', 800),
(8, '2022-02-10', 'Radar', 400),
(9, '2022-02-07', 'Dharmaniapps', 200),
(10, '2022-02-26', 'Hosting site', 250);

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `report_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `team_id` int(55) NOT NULL,
  `user_id` varchar(225) NOT NULL,
  `role_id` int(55) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `tasks` text NOT NULL,
  `hrs` int(2) NOT NULL,
  `report_status` int(55) NOT NULL,
  `noti_id` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`report_id`, `date`, `team_id`, `user_id`, `role_id`, `project_name`, `tasks`, `hrs`, `report_status`, `noti_id`) VALUES
(15, '2022-02-23', 1, '27', 2, '4', 'Content update', 2, 1, 1),
(16, '2022-02-10', 1, '27', 2, '8', 'get an app access design', 3, 2, 5),
(17, '2022-02-22', 1, '27', 2, '5', 'Design updates', 4, 3, 4),
(19, '2022-02-24', 1, '27', 2, '4', 'Add remove button with ask reason functionality', 3, 2, 5),
(20, '2022-02-24', 1, '27', 2, '4', 'Site Live', 1, 2, 5),
(22, '2022-02-26', 1, '28', 3, '5', 'design issues', 2, 2, 1),
(23, '2022-02-26', 1, '28', 3, '9', 'project complete', 8, 0, 1),
(24, '2022-02-26', 1, '28', 3, '9', 'project complete', 8, 0, 1),
(25, '2022-02-26', 1, '28', 3, '9', 'project complete', 8, 0, 1),
(26, '2022-02-19', 1, '28', 3, '10', 'complete', 8, 0, 1),
(27, '2022-02-26', 1, '28', 3, '9', 'project complete', 8, 0, 1),
(28, '2022-02-26', 1, '28', 3, '9', 'project complete', 8, 0, 1),
(29, '2022-02-26', 1, '28', 3, '7', 'complete', 8, 2, 1),
(30, '2022-02-26', 1, '28', 3, '5', 'complete', 8, 2, 1),
(31, '2022-02-24', 1, '27', 2, '5', 'complete', 4, 1, 1),
(32, '2022-02-18', 1, '28', 3, '7', 'complete ', 5, 2, 1),
(33, '2022-02-26', 1, '28', 3, '9', 'Website Project Completed', 8, 2, 1),
(34, '2022-02-25', 1, '28', 3, '8', 'design isseues', 4, 2, 1),
(35, '2022-02-26', 1, '27', 2, '7', 'homepage redesign complete', 8, 1, 1),
(36, '2022-02-07', 1, '28', 3, '9', 'Design Complete<br>\r\nResponsive<br>\r\nMade site live<br>', 7, 2, 1),
(37, '2022-02-26', 1, '28', 3, '6', 'task 1 <br> task 2 <br> task 3 <br>', 3, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `report_status`
--

CREATE TABLE `report_status` (
  `status_id` int(11) NOT NULL,
  `status` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report_status`
--

INSERT INTO `report_status` (`status_id`, `status`) VALUES
(1, 'pending'),
(2, 'approved'),
(3, 'rejected');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `rol_id` int(11) NOT NULL,
  `role` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`rol_id`, `role`) VALUES
(1, 'super admin'),
(2, 'team member'),
(3, 'team leader');

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `team_name`) VALUES
(1, 'frontend development'),
(2, 'graphics designing'),
(3, 'android development'),
(4, 'ios development'),
(5, 'backend development');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(1) NOT NULL,
  `team` int(1) NOT NULL,
  `notifi_id` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password`, `role`, `team`, `notifi_id`) VALUES
(1, 'superadmin', 'superadmin@gmail.com', '17c4520f6cfd1ab53d8745e84681eb49', 1, 0, 3),
(7, 'vishal', 'dharmaniz.vishal@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 3, 3),
(8, 'Gurdeep', 'dharmaniz.gurdeep@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 2, 3),
(9, 'Sushant', 'dharmaniz.sushant@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 2, 3),
(11, 'Mahima', 'dharmaniz.mahima@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 3, 3),
(13, 'Mandeep', 'dharmaniz.mandeep@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 5, 3),
(14, 'Sai', 'dharmaniz.sai@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 5, 3),
(15, 'Kusum', 'dharmaniz.kusum@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 5, 3),
(16, 'Komal', 'dharmaniz.komal@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 5, 3),
(19, 'Abhishek', 'dharmaniz.abhishek@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 5, 3),
(22, 'Atal', 'dharmaniz.atalbihari@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 5, 3),
(27, 'Gurpreet', 'dharmaniz.gurpreet@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 1, 3),
(28, 'Karishma', 'dharmaniz.karishma@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 1, 3),
(29, 'Ramandhiman', 'dharmaniz.ramandhiman@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 3, 3, 3),
(33, 'Vikas', 'dharmaniz.vikas@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 2, 3, 3),
(34, 'tester', 'tester@gmail.com', '4297f44b13955235245b2497399d7a93', 2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`notification_id`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`projects_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `report_status`
--
ALTER TABLE `report_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`rol_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `notification_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `projects_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `report_status`
--
ALTER TABLE `report_status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `rol_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
