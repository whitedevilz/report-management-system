<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/styles.css">

    <title>Report Managment</title>
  </head>
  <body>

    <section id="login-form">
      <div class="d-flex flex-column w-100 justify-content-center align-items-center vh-100">
        <div class="p-4 rounded shadow w-25">
          <h4 class="text-center mb-3">Welcome Back</h4>
          <div>
            <form action="" method="POST">
              <div class="form-group">
                <label for="username">Email address</label>
                <input type="email" name="username" id="username" class="form-control">
              </div>
              <div class="form-group">
                <label for="password">password</label>
                <input type="password" name="password" id="password" class="form-control">
              </div>
              <div class="form-group text-center">
                <input type="submit" name="login" class="btn btn-dark" value="Login" style="min-width: 200px;">
              </div>
            </form>

            <?php
              if (isset($_POST['login'])) {
                include "src/config.php";

                $username = $_POST['username'];
                $password = $_POST['password'];

                $password = md5($password);

                $sql = "SELECT id, username, role, team FROM user WHERE email = '$username' AND password = '$password'";
                $result = mysqli_query($conn, $sql) or die("Query Failed");

                if (mysqli_num_rows($result) > 0) {
                  while ($row = mysqli_fetch_assoc($result)) {
                    session_start();

                    $_SESSION['userid'] = $row['id'];
                    $_SESSION['username'] = $row['username'];
                    $_SESSION['user_id'] = $row['id'];
                    $_SESSION['role'] = $row['role'];
                    $_SESSION['team'] = $row['team'];

                    header("location: dashboard.php");
                  }
                }
                else {
                  echo '<div class="alert alert-danger">Invalid Credentials</div>';
                }

              }
            ?>

          </div>
        </div>
      </div>
    </section>


    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"></script>
  </body>
</html>
