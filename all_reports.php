<?php

    include_once ("src/header.php");

    include_once ("header.php");
    if($_SESSION['role'] == 1){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE report_status.status_id = 2 ORDER BY reports.date DESC";
    }
    if($_SESSION['role'] == 3){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id  WHERE reports.team_id = $_SESSION[team] AND report_status.status_id = 2 ORDER BY reports.date DESC";
    }
    if($_SESSION['role'] == 2){
        $sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE reports.team_id = $_SESSION[team] AND reports.role_id = $_SESSION[role] ORDER BY reports.date DESC";
    }
    $result = mysqli_query($conn, $sql);

    $sql2 = "SELECT report_status FROM reports INNER JOIN report_status ON report_status.status_id = reports.report_status WHERE report_status.status_id = 1";
    $countrow = mysqli_query($conn, $sql2);

?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">ALL Reports</h1>
                        <div class="d-flex">
                          <?php if ($_SESSION['role'] == 1) {
                          ?>
                          <select class="form-control mr-3 text-capitalize" id="fetchresult" style="min-width: 130px;">
                            <option selected value="0">All</option>
                            <?php
                              $query = "SELECT * FROM teams";
                              $res = mysqli_query($conn, $query);
                              while ($row2 = $res->fetch_assoc()) {
                                echo '<option class="text-capitalize" value="'.$row2['team_id'].'"> '.$row2['team_name'].'</option>';
                              }
                            ?>
                          </select>
                          <?php } ?>
                          <a href="export.php"><button type="button" class="btn btn-primary">Export</button></a>
                      </div>
                    </div>

                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                      <li class="nav-item" role="presentation">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-all_report" role="tab" aria-controls="pills-home" aria-selected="true">All Reports</a>
                      </li>
                      <?php
                        if($_SESSION['role'] == 3){
                      ?>
                      <li class="nav-item" role="presentation">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-pending_report" role="tab" aria-controls="pills-profile" aria-selected="false">Pending Approve <sup class="text-danger" style="font-size: 16px;"><?php if(mysqli_num_rows($countrow) == 0){echo "";}else{echo mysqli_num_rows($countrow);} ?></sup></a>
                      </li>
                    <?php
                      }
                     ?>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                      <div class="tab-pane fade show active" id="pills-all_report" role="tabpanel" aria-labelledby="pills-home-tab">
                        <!-- Content Row -->
                        <div class="row">

                            <!-- Content Column -->
                            <div class="col-md-12 mb-4">

                                <!-- Project Card Example -->
                                <div class="card shadow mb-4">
                                    <div class="card-body">
                                        <table class="table table-striped" id="table_id">
                                          <thead>
                                            <tr>
                                              <th scope="col">#</th>
                                              <th scope="col">Date</th>
                                                <?php
                                                    if($_SESSION['role'] == 1){
                                                ?>
                                                <th scope="col">Employee Name</th>
                                                <?php
                                                    }
                                                ?>
                                                <?php
                                                    if($_SESSION['role'] == 3){
                                                ?>
                                                <th scope="col">Team Member</th>
                                                <?php
                                                    }
                                                ?>
                                              <th scope="col">Project Name</th>
                                              <th scope="col">Tasks</th>
                                               <th scope="col">Hrs</th>
                                                <?php
                                                    if($_SESSION['role'] == 2){
                                                ?>
                                               <th>Status</th>
                                                <th>Action</th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                                if($result) {
                                                    while($row = $result->fetch_assoc()){
                                            ?>
                                              <tr>
                                              <th scope="row" class="rowNo"></th>
                                              <td><?php echo $row['date']; ?></td>
                                              <?php
                                                if($_SESSION['role'] == 3 || $_SESSION['role'] == 1){
                                                ?>
                                                <td scope="col"><a href="single_user_reports.php?id=<?php echo $row['id']?>&user=<?php echo $row['username']; ?>"><?php echo $row['username']; ?></a></td>
                                                <?php
                                                    }
                                                ?>
                                              <td><a href="single_project_report.php?pid=<?php echo $row['projects_id']; ?>&pname=<?php echo $row['projects_name']; ?>"><?php echo $row['projects_name']; ?></a></td>
                                              <td><?php echo $row['tasks']; ?></td>
                                              <td><?php echo $row['hrs']." hrs"; ?></td>
                                                  <?php
                                                    if($_SESSION['role'] == 2){
                                                ?>
                                                <?php
                                                  if ($row['status_id'] == 1) {
                                                ?>
                                                  <td class="text-warning text-capitalize"><i class="fa fa-exclamation-circle"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                  if ($row['status_id'] == 2){
                                                ?>
                                                  <td class="text-success text-capitalize"><i class="fa fa-check-circle"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                ?>
                                                <?php
                                                  if ($row['status_id'] == 3){
                                                ?>
                                                  <td class="text-danger text-capitalize"><i class="fa fa-ban"></i> <?php echo $row['status']; ?></td>
                                                <?php
                                                  }
                                                ?>
                                              <td class="text-center">
                                                <?php if ($row['status_id'] == 2){ ?>
                                                  <button class="btn btn-sm btn-primary" disabled>Edit</button>
                                                  <button class="btn btn-sm btn-danger" disabled>Delete</button>
                                              <?php }else{ ?>
                                                  <a href="edit_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-primary">Edit</a>
                                                  <a href="delete_report.php?id=<?php echo $row['report_id']; ?>" class="btn btn-sm btn-danger">Delete</a>
                                                <?php } ?>
                                              </td>
                                                  <?php
                                                    }
                                                ?>
                                            </tr>

                                              <?php

                                                    }
                                                }
                                              ?>
                                          </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                      </div>

                      <?php
                        if ($_SESSION['role'] == 3) {
                      ?>
                      <div class="tab-pane fade" id="pills-pending_report" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <!-- Content Row -->
                        <div class="row">

                            <!-- Content Column -->
                            <div class="col-md-12 mb-4">

                                <!-- Project Card Example -->
                                <div class="card shadow mb-4">
                                    <div class="card-body">
                                        <table class="table table-striped" id="table_id">
                                          <thead>
                                            <tr>
                                              <th scope="col">#</th>
                                              <th scope="col">Date</th>
                                                <?php
                                                    if($_SESSION['role'] == 1){
                                                ?>
                                                <th scope="col">Employee Name</th>
                                                <?php
                                                    }
                                                ?>
                                                <?php
                                                    if($_SESSION['role'] == 3){
                                                ?>
                                                <th scope="col">Team Member</th>
                                                <?php
                                                    }
                                                ?>
                                              <th scope="col">Project Name</th>
                                              <th scope="col">Tasks</th>
                                               <th scope="col">Hrs</th>
                                                <?php
                                                    if($_SESSION['role'] != 1){
                                                ?>
                                                <th>Action</th>
                                                <?php
                                                    }
                                                ?>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              <?php
                                              if($_SESSION['role'] == 3){
                                                  $sql2 = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id  WHERE reports.team_id = $_SESSION[team] AND report_status.status_id = 1 ORDER BY reports.date DESC";
                                              }
                                              $result2 = mysqli_query($conn, $sql2);
                                                if($result2) {
                                                    while($row = $result2->fetch_assoc()){
                                            ?>
                                              <tr>
                                              <th scope="row" class="rowNo"></th>
                                              <td><?php echo $row['date']; ?></td>
                                              <?php
                                                if($_SESSION['role'] == 3 || $_SESSION['role'] == 1){
                                                ?>
                                                <td scope="col"><?php echo $row['username']; ?></td>
                                                <?php
                                                    }
                                                ?>
                                              <td><?php echo $row['projects_name']; ?></td>
                                              <td><?php echo $row['tasks']; ?></td>
                                              <td><?php echo $row['hrs']." hrs"; ?></td>
                                                  <?php
                                                    if($_SESSION['role'] != 1){
                                                ?>
                                              <td class="text-center">
                                                  <a href="update_status.php?id=<?php echo $row['report_id']; ?>&status=2" class="btn btn-sm btn-success">Approve</a>
                                                  <a href="update_status.php?id=<?php echo $row['report_id']; ?>&status=3" class="btn btn-sm btn-danger">Reject</a>
                                              </td>
                                                  <?php
                                                    }
                                                ?>
                                            </tr>

                                              <?php

                                                    }
                                                }
                                              ?>
                                          </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                      </div>
                      <?php
                        }
                      ?>
                    </div>



                </div>
                <!-- /.container-fluid -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
<script type="text/javascript">
  $(document).ready(function(){
    $("#fetchresult").on('change', function(){
      var value = $(this).val();
      // alert(value);

      $.ajax({
        url: "filter_report.php",
        type: "POST",
        data: 'request=' + value,
        success: function(data){
          $("#table_id").html(data);
        }
      });
    });
  });
</script>
