<?php

    include_once ("src/header.php");

    include_once ("header.php");

?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <?php
                            if ($_SESSION['role'] == 1) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">ALL Employee</h1>
                        <?php
                            }
                        ?>
                        <?php
                            if ($_SESSION['role'] == $_SESSION['role'] && $_SESSION['role'] != 1) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">ALL Team Members</h1>
                        <?php
                            }
                        ?>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <table class="table table-striped" id="table_id">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Project Name</th>
                                          <th scope="col">Date</th>
                                          <th scope="col">Hrs</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php

                                            include "src/config.php";
                                            if ($_SESSION['role'] == 1){
                                                $sql = "SELECT * FROM projects ORDER BY project_date DESC";
                                                $result = mysqli_query($conn, $sql);

                                                if ($result) {

                                                    while ($row = $result->fetch_assoc()) {

                                                ?>
                                                <tr>
                                                  <th scope="row" class="rowNo"></th>
                                                  <td class="text-capitalize"><?php echo $row['projects_name'];?></td>
                                                  <td><?php echo $row['project_date']; ?></td>
                                                  <td class="text-capitalize"><?php echo $row['pro_hrs']." hrs"; ?></td>
                                                </tr>
                                            <?php
                                                    }
                                                    /* free result set */
                                                    $result->free();
                                                }
                                            }
                                        ?>
                                      </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
