<?php

    include_once ("src/header.php");

    if (isset($_SESSION['role']) && $_SESSION['role'] == 2) {
      header ("location: dashboard.php");
      die();
    }
    include_once ("header.php");

?>



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <?php
                            if ($_SESSION['role'] == 1) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">Add Employee</h1>
                        <?php
                            }
                        ?>
                        <?php
                            if ($_SESSION['role'] == $_SESSION['role']) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">Add Team Member</h1>
                        <?php
                            }
                        ?>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class="m-auto">
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" name="username" id="username" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="email" name="email" id="email" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" name="password" id="password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label for="con_password">Confirm Password</label>
                                                <input type="password" name="con_password" id="con_password" class="form-control">
                                            </div>
                                            <?php
                                                if ($_SESSION['role'] == 1) {
                                            ?>
                                            <div class="form-group">
                                                <label for="role">Select Role</label>
                                                <select class="form-control" name="role" id="role">
                                                    <option selected disabled>Select One</option>
                                                    <option value="2">Employee</option>
                                                    <option value="3">Team Leader</option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="team">Select Team</label>
                                                <select class="form-control" name="team" id="team">
                                                    <option selected disabled>Select One</option>
                                                    <option value="1">Front-End Development</option>
                                                    <option value="2">Graphics Designing</option>
                                                    <option value="3">Android Development</option>
                                                    <option value="4">IOS Development</option>
                                                    <option value="5">Back-End Development</option>
                                                </select>
                                            </div>
                                            <?php
                                                }
                                            ?>
                                            <?php
                                                if ($_SESSION['role'] == 1) {
                                            ?>
                                            <div class="form-group">
                                                <input type="submit" name="add_user" class="btn btn-dark" value="Add Employee">
                                            </div>
                                            <?php
                                                }
                                            ?>
                                            <?php
                                                if ($_SESSION['role'] == $_SESSION['role'] && $_SESSION['role'] != 1) {
                                            ?>
                                            <div class="form-group">
                                                <input type="submit" name="add_user" class="btn btn-dark" value="Add Team Member">
                                            </div>
                                            <?php
                                                }
                                            ?>
                                        </form>

                                        <!-- PHP -->
                                        <?php

                                        if (isset($_POST['add_user'])) {
                                            include "src/config.php";

                                            $username = $_POST["username"];
                                            $email = $_POST["email"];
                                            $password = $_POST["password"];
                                            $img = "undraw_profile.svg";
                                            $con_password = $_POST["con_password"];
                                            if ($_SESSION['role'] == 1) {
                                              $role = $_POST["role"];
                                              $team = $_POST["team"];
                                            }

                                            if (!empty($username) || !empty($email) || !empty($password) || !empty($role) || !empty($team)) {

                                                if ($password == $con_password) {
                                                    $password = md5($password);

                                                    if ($_SESSION['role'] == 1) {

                                                        $sql = "INSERT INTO user(username, email, password, role, team, notifi_id, img) VALUES ('$username','$email','$password','$role','$team',5,'$img')";

                                                        $result = mysqli_query($conn, $sql) or die("Query Failed");

                                                        if ($result) {
                                                            echo '<div class="alert alert-success">New Employee Added.</div>';
                                                        }
                                                    }

                                                    else if ($_SESSION['role'] != 1) {
                                                        $sql = "INSERT INTO user(username, email, password, role, team, notifi_id, img) VALUES ('$username','$email','$password',2,'$_SESSION[team]',4,'$img')";

                                                        $result = mysqli_query($conn, $sql) or die("Query Failed");

                                                        if ($result) {
                                                            echo '<div class="alert alert-success">New Team Member Added '.$username.'.</div>';
                                                        }
                                                    }
                                                }
                                                else {
                                                    echo '<div class="alert alert-danger">Password not match.</div>';
                                                }

                                            }
                                            else {
                                                echo '<div class="alert alert-danger">All Fields Required.</div>';
                                            }
                                        }

                                        ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
