<?php

include 'src/config.php';

$sql = "SELECT * FROM reports INNER JOIN projects ON reports.project_name = projects.projects_id INNER JOIN teams ON teams.team_id = reports.team_id INNER JOIN user ON user.id = reports.user_id INNER JOIN role ON role.rol_id = reports.role_id INNER JOIN report_status ON reports.report_status = report_status.status_id WHERE report_status.status_id = 2 ORDER BY reports.date DESC";
$result = mysqli_query($conn, $sql);
$html = '<table cellpadding="8"><tr><th>date</th><th>Username</th><th>Email</th><th>Project</th><th>Task</th><th>Hrs</th></tr>';
while ($row = mysqli_fetch_assoc($result)) {
  $html.='<tr><td>'.$row['date'].'</td><td>'.$row['username'].'</td><td>'.$row['email'].'</td><td>'.$row['projects_name'].'</td><td>'.$row['tasks'].'</td><td>'.$row['hrs'].' hrs</td></tr>';
}
$html.='</table>';
header('Content-Type:application/xls');
header('Content-Disposition:attachment;filename=report.xls');
echo $html;

?>
