<?php

    include_once ("src/header.php");
    include_once ("header.php");


    include "src/config.php";
    $sql = "SELECT * FROM user INNER JOIN role ON user.role = role.rol_id INNER JOIN teams ON user.team = teams.team_id WHERE teams.team_id = $_GET[id] ORDER BY user.role DESC";


    $result = mysqli_query($conn, $sql);

?>

    <!-- Begin Page Content -->
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800 text-capitalize"><?php echo $_GET['title']; ?></h1>
        </div>

        <!-- Content Row -->
        <div class="row">

            <!-- Content Column -->
            <div class="col-md-12 mb-4">

                <!-- Project Card Example -->
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <table class="table table-striped" id="table_id">
                          <thead>
                            <tr>
                              <th scope="col">#</th>
                              <th scope="col">Username</th>
                              <th scope="col">Email</th>
                              <th scope="col">Role</th>
                            </tr>
                          </thead>
                          <tbody>
                          <?php

                              if ($result) {
                                while ($row = $result->fetch_assoc()) {
                          ?>
                            <tr>
                              <th scope="row" class="rowNo"></th>
                              <td class="text-capitalize"><?php echo $row['username']; ?></td>
                              <td><?php echo $row['email']; ?></td>
                              <td class="text-capitalize"><?php echo $row['role']; ?></td>
                            </tr>
                          <?php
                                    }

                                    /* free result set */
                                    $result->free();
                                }

                           ?>
                          </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>

    </div>
    <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
