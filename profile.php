<?php
    include_once ("src/header.php");
    include_once ("header.php");

    $username = $_SESSION['username'];
    $userid = $_SESSION['userid'];

    $sql = "SELECT * FROM user INNER JOIN teams ON user.team = teams.team_id WHERE username = '$username'";
    $result = mysqli_query($conn, $sql);
    if ($result) {
      while ($row = $result->fetch_assoc()) {
        $username = $row['username'];
        $email = $row['email'];
        $team = $row['team_name'];
        $img = $row['img'];
      }
    }
?>

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Profile</h1>
    </div>

    <!-- Content Row -->
    <div class="row">
        <div class="col-md-3 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="profile text-center">
                    <img class="rounded-circle" src="src/upload/<?php echo $img; ?>" id="profile_img" alt="profile image" width="200" height="200">
                    <h4 class="mt-2"><?php echo $username; ?></h4>
                    <?php
                      if ($_SESSION['role'] != 1) {
                    ?>
                        <p class="m-0"><?php echo $email; ?></p>
                        <p class="text-capitalize"><?php echo $team; ?></p>
                    <?php
                      }
                    ?>

                  </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 mb-4">
            <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="change_pass">
                    <form action="" method="post">
                      <h4 class="mb-4">Change Password</h4>
                      <div class="form-group">
                        <label for="new_pass">New Password</label>
                        <input type="Password" name="new_pass" id="new_pass" class="form-control">
                      </div>
                      <div class="form-group">
                        <label for="con_pass">Confirm Password</label>
                        <input type="Password" name="con_pass" id="con_pass" class="form-control">
                      </div>
                      <div class="form-group">
                        <input type="submit" name="change_pass" value="Change Password" class="btn btn-dark">
                      </div>
                    </form>

                    <?php
                      if (isset($_POST['change_pass'])) {
                        if ($_POST['new_pass'] == $_POST['con_pass']) {
                          $newPassword = md5($_POST['new_pass']);
                          $sql = "UPDATE user SET password = '$newPassword' WHERE id = $userid";
                          $result = mysqli_query($conn, $sql);
                          if ($result) {
                            echo '<div class="alert alert-success">Password Updated</div>';
                            session_unset();
                          	session_destroy();
                          }
                        }
                        else {
                          echo '<div class="alert alert-danger">Password not match</div>';
                        }
                      }
                    ?>
                  </div>
                </div>
            </div>

            <div class="card shadow mb-4">
                <div class="card-body">
                  <div class="update_profile_img">
                    <form action="upload.php" method="post" enctype="multipart/form-data">
                      <div class="form-group">
                        <label for="fileToUpload">Browse Profile Picture</label>
                        <input type="file" name="fileToUpload" class="form-control" id="fileToUpload">
                      </div>
                      <div class="form-group">
                        <input type="submit" value="Update Image" class="btn btn-dark">
                      </div>
                    </form>
                  </div>
                </div>
            </div>
        </div>
      </div>
</div>

<?php
    include_once ("src/footer.php");
    include_once ("footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    $("#profile_img").on('change', function(){
      var value = $(this).val();
      // alert(value);

      $.ajax({
        url: "upload.php",
        type: "POST",
        data: 'request=' + value,
        success: function(data){
          $("#table_id").attr("src", data);
        }
      });
    });
  });
</script>
