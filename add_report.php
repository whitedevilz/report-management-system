<?php

    include_once ("src/header.php");

    include_once ("header.php");

?>



                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <h1 class="h3 mb-0 text-gray-800">Add Report</h1>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <div class=" m-auto">
                                        <form action="" method="POST">
                                            <div class="form-group">
                                                <label for="date">Date</label>
                                                <input type="date" class="form-control" id="date" name="date">
                                            </div>
                                            <div class="form-group">
                                                <label for="pro_name">Project Name</label>
                                                <select id="pro_name" name="pro_name" class="form-control">
                                                    <option selected disabled>Select One</option>
                                                    <?php
                                                        $sql1 = "SELECT * FROM projects";
                                                        $result1 = mysqli_query($conn, $sql1);
                                                        while($row = $result1->fetch_assoc()) {
                                                            echo '<option value="'.$row['projects_id'].'">'.$row['projects_name'].'</option>';
                                                        }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="tasks">Tasks</label><br>
                                                <small>Please use <strong>&lt;br&gt;</strong> to seprate task's.</small>
                                                <textarea class="form-control" id="tasks" name="tasks" rows="4" placeholder="Task 1 <br> \nTask 2 <br> \nTask 3 <br>"></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label for="hrs">Hrs</label>
                                                <input type="number" class="form-control" id="hrs" name="hrs" step="any">
                                            </div>
                                            <div class="form-group">
                                                <input type="submit" name="add_report" class="btn btn-dark" value="Add Report">
                                            </div>
                                        </form>

                                        <!-- PHP -->
                                        <?php

                                        if (isset($_POST['add_report'])) {
                                            include "src/config.php";

                                            $date = $_POST["date"];
                                            $pro_name = $_POST["pro_name"];
                                            $tasks = $_POST["tasks"];
                                            $hrs = $_POST["hrs"];

                                            if (!empty($date) || !empty($pro_name) || !empty($tasks) || !empty($hrs)) {
                                              if ($_SESSION['role'] == 3) {
                                                $sql = "INSERT INTO reports(date, team_id, user_id, role_id, project_name, tasks, hrs, report_status, noti_id) VALUES ('$date','$_SESSION[team]','$_SESSION[userid]', '$_SESSION[role]', '$pro_name','$tasks','$hrs',2,1)";
                                              }
                                              if ($_SESSION['role'] == 2) {
                                                $sql = "INSERT INTO reports(date, team_id, user_id, role_id, project_name, tasks, hrs, report_status, noti_id) VALUES ('$date','$_SESSION[team]','$_SESSION[userid]', '$_SESSION[role]', '$pro_name','$tasks','$hrs',1,1)";
                                              }

                                                $result = mysqli_query($conn, $sql) or die("Query Failed");

                                                if ($result) {
                                                    echo '<div class="alert alert-success">New Report Added.</div>';
                                                }
                                                else {
                                                    echo '<div class="alert alert-danger">All Fields Required.</div>';
                                                }

                                            }
                                            else {
                                                echo '<div class="alert alert-danger">All Fields Required.</div>';
                                            }
                                        }

                                        ?>

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
