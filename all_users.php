<?php

    include_once ("src/header.php");

    include_once ("header.php");

?>


                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <div class="d-sm-flex align-items-center justify-content-between mb-4">
                        <?php
                            if ($_SESSION['role'] == 1) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">ALL Employee</h1>
                        <?php
                            }
                        ?>
                        <?php
                            if ($_SESSION['role'] == $_SESSION['role'] && $_SESSION['role'] != 1) {
                        ?>
                        <h1 class="h3 mb-0 text-gray-800">ALL Team Members</h1>
                        <?php
                            }
                        ?>
                    </div>

                    <!-- Content Row -->
                    <div class="row">

                        <!-- Content Column -->
                        <div class="col-md-12 mb-4">

                            <!-- Project Card Example -->
                            <div class="card shadow mb-4">
                                <div class="card-body">
                                    <table class="table table-striped" id="table_id">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Username</th>
                                          <th scope="col">Email</th>
                                          <th scope="col">Role</th>
                                          <th scope="col">Team</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        <?php

                                            include "src/config.php";
                                            if ($_SESSION['role'] == 1){
                                                $sql = "SELECT * FROM user INNER JOIN role ON user.role = role.rol_id INNER JOIN teams ON user.team = teams.team_id";
                                                $result = mysqli_query($conn, $sql);

                                                if ($result) {

                                                    while ($row = $result->fetch_assoc()) {

                                                ?>
                                                <tr>
                                                  <th scope="row" class="rowNo"></th>
                                                  <td class="text-capitalize"><?php echo $row['username'];?></td>
                                                  <td><?php echo $row['email']; ?></td>
                                                  <td class="text-capitalize"><?php echo $row['role']; ?></td>
                                                    <?php
                                                        if ($_SESSION['role'] == 1){
                                                    ?>
                                                    <td class="text-capitalize"><a href="team_view.php?id=<?php echo $row['team']; ?>&title=<?php echo $row['team_name']; ?>" name="team_view"><?php echo $row['team_name']; ?></a></td>
                                                    <?php
                                                        }
                                                        else{
                                                    ?>
                                                    <td class="text-capitalize"><?php echo $row['team_name']; ?></td>
                                                    <?php
                                                        }
                                                    ?>

                                                </tr>
                                            <?php

                                                    }

                                                    /* free result set */
                                                    $result->free();
                                                }
                                            }
                                            if ($_SESSION['role'] == $_SESSION['role'] && $_SESSION['role'] != 1){

                                            $sql = "SELECT * FROM user INNER JOIN role ON user.role = role.rol_id INNER JOIN teams ON user.team = teams.team_id WHERE teams.team_id = $_SESSION[team] ORDER BY user.role DESC";
                                            $result = mysqli_query($conn, $sql);

                                            if ($result) {

                                                    while ($row = $result->fetch_assoc()) {

                                            ?>
                                        <tr>
                                          <th scope="row" class="rowNo"></th>
                                          <td class="text-capitalize"><a href="single_user_reports.php?id=<?php echo $row['id']?>&user=<?php echo $row['username']; ?>"><?php echo $row['username'];?></a></td>
                                          <td><?php echo $row['email']; ?></td>
                                          <td class="text-capitalize"><?php echo $row['role']; ?></td>
                                          <td class="text-capitalize"><?php echo $row['team_name']; ?></td>
                                        </tr>
                                        <?php

                                            }

                                            /* free result set */
                                            $result->free();
                                        }
                                    }

                                        ?>
                                      </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->


<?php

    include_once ("footer.php");

    include_once ("src/footer.php");

?>
